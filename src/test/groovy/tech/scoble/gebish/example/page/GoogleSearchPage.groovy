package tech.scoble.gebish.example.page

import geb.Page

/**
 * Created by Scott Scoble on 2/29/2016.
 */
class GoogleSearchPage extends Page {
    static url = "https://google.com"
    static at = { searchBox.isDisplayed() }
    static content = {
        searchBox { $("input[name=q]") }
        results(wait: true) { $("div.rc") }
        result { index -> results[index] }
    }
}
