package tech.scoble.gebish.example.spec

import geb.spock.GebSpec
import tech.scoble.gebish.example.page.GoogleSearchPage

/**
 * Created by Scott Scoble on 2/29/2016.
 */
class ExampleTwoSpec extends GebSpec {
    def "searching for lolcats gives me lolcats"() {
        given:
        to GoogleSearchPage

        when:
        searchBox.value("lolcats")

        then:
        result(0).text().toLowerCase().contains("lolcats")
    }
}
