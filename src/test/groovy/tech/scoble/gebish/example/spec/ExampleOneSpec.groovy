package tech.scoble.gebish.example.spec

import geb.spock.GebSpec

/**
 * Created by Scott Scoble on 2/29/2016.
 */
class ExampleOneSpec extends GebSpec {
    def "searching for lolcats gives me lolcats"() {
        given:
        go "https://google.com"

        when:
        $("input[name=q]").value("lolcats")

        then:
        waitFor(10) {
            $("div.rc").size() > 0
        }
        $("div.rc", 0).text().toLowerCase().contains('lolcats')
    }
}
